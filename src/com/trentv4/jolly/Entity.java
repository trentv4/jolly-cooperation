package com.trentv4.jolly;

import com.trentv4.pliable.Renderer;
import com.trentv4.jolly.Action;
import static com.trentv4.jolly.Action.*;

public class Entity 
{
	//Player variables
	public String texture = "";
	public int x = 0;
	public int y = 0;
	public int hp = 171;
	public int maxHP = 371;
	public int stamina = 189;
	public int maxStamina = 189;
	public int estus = 5;
	public int speed = 2;
	
	public int[] direction = {0,0};
	
	//Action variables
	public int delay = 0;
	public Action currentAction = NONE;
	public int data = -1;
	
	public Entity(String texture, int x, int y)
	{
		this.x = x;
		this.y = y;
		this.texture = texture;
		if(texture.equals("sword.png"))
		{
			
		}
		else if(texture.equals("pyro.png"))
		{
			
		}
	}
	
	public void setAction(Action action, int delay, int data)
	{
		this.delay = delay;
		this.currentAction = action;
		this.data = data;
	}
	
	public void tick()
	{
		if(delay > 0) { delay--; }
		if(delay == 1)
		{
			doAction();
		}
		if(delay == 0)
		{
			this.x += direction[0] * speed;
			this.y += direction[1] * speed;
		}
	}

	public void doAction()
	{
		if(currentAction == HEAL)
		{
			hp += 100;
			if(hp > maxHP) { hp = maxHP; };
			estus--;
		}
		if(currentAction == SWING)
		{
			
		}
		if(currentAction == CAST)
		{
			
		}
		if(currentAction == ROLL)
		{
			
		}
		currentAction = NONE;
	}
	
	public void render(Renderer renderer)
	{
		renderer.drawImage(texture, x, y, 32, 48);
	}	
}
