package com.trentv4.jolly;

public enum Action {
	NONE, HEAL, SWING, CAST, ROLL
}