package com.trentv4.jolly;

import java.awt.Color;

import com.trentv4.pliable.DisplayManager;
import com.trentv4.pliable.GameStructure;
import com.trentv4.pliable.Renderer;

public class GameStructureJolly extends GameStructure
{
	public int[] camera = {0,0};
	public boolean hasFightBegun = false;
	public Entity me = new Entity("sword.png", 200, 200);
	public Entity friend = new Entity("null.png", 200, 200);
	
	public void initialize()
	{
		this.INPUT_SCENARIO = new InputScenarioJolly();
	}

	public void draw(Renderer renderer)
	{
		renderer.drawRectangle(Color.blue, 0, 0, DisplayManager.width, DisplayManager.height);
		renderer.drawImage("overlay.png", 0, 0, DisplayManager.width, DisplayManager.height);

		renderer.drawRectangle(Color.black, 72, 27, 371, 7);
		renderer.drawRectangle(Color.black, 72, 38, 189, 7);
		renderer.drawImage("health.png", 72, 27, me.hp, 7);   //length: 371
		renderer.drawImage("stamina.png", 72, 38, me.stamina, 7);  //length: 189

		renderer.drawImage(me.estus + ".png", 103, 454, 5, 9);		
		
		me.render(renderer);
		friend.render(renderer);
	}

	public void tick()
	{
		me.tick();
		friend.tick();
	}
}
