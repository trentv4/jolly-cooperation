package com.trentv4.jolly;

import static com.trentv4.pliable.GameLoop.a;
import static com.trentv4.pliable.InputMapper.isDown;
import static com.trentv4.pliable.InputMapper.isPressed;

import org.lwjgl.glfw.GLFW;

import com.trentv4.pliable.GameLoop;
import com.trentv4.pliable.InputScenario;

public class InputScenarioJolly extends InputScenario 
{
	public void tick()
	{
		if(isPressed(GLFW.GLFW_KEY_1))
		{
			if(!GameLoop.a.hasFightBegun)
			{
				a.me = new Entity("sword.png", a.me.x, a.me.y);
			}
		}
		if(isPressed(GLFW.GLFW_KEY_2))
		{
			if(!GameLoop.a.hasFightBegun)
			{
				a.me = new Entity("pyro.png", a.me.x, a.me.y);
			}			
		}
		if(isPressed(GLFW.GLFW_KEY_F))
		{
			if(a.me.estus > 0)
			{
				a.me.setAction(Action.HEAL, 100, 0);
			}
		}
		boolean x = false;
		boolean y = false;
		if(isDown(GLFW.GLFW_KEY_W))
		{
			a.me.direction[1] = -1;
			y = true;
		}
		if(isDown(GLFW.GLFW_KEY_A))
		{
			a.me.direction[0] = -1;
			x = true;
		}
		if(isDown(GLFW.GLFW_KEY_S))
		{
			a.me.direction[1] = 1;
			y = true;
		}
		if(isDown(GLFW.GLFW_KEY_D))
		{
			a.me.direction[0] = 1;
			x = true;
		}
		if(!x) { a.me.direction[0] = 0; }
		if(!y) { a.me.direction[1] = 0; }
	}
}
